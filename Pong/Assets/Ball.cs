﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	public float speed = 30;

	void Start () {
		//Initial Velocity
		GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
	}
	
	void OnCollisionEnter2D(Collision2D col) {
		Debug.Log ("Collided");
		//Note: col holds the collision information
		//Ball collided with a racket, then:
		//col.gameObject is the racket
		//col.transform.position is the racket's position
		//col.collider is the racket's collider

		//Hit the left racket?
		if (col.gameObject.name == "RacketLeft") {
			//calculate hit Factor
			float y = hitFactor (transform.position,
				          col.transform.position,
				          col.collider.bounds.size.y);

			//Calculate direction, make length=1 via .normalized
			Vector2 dir = new Vector2 (1, y).normalized;

			//Set Velocity with dir * speed
			GetComponent<Rigidbody2D> ().velocity = dir * speed;
		}

		// Hit the right Racket?
		if (col.gameObject.name == "RacketRight") {
			//calculate hit factor
			float y = hitFactor (transform.position,
				          col.transform.position,
				          col.collider.bounds.size.y);
			//calculate direction, make lenght=1 via .normalized
			Vector2 dir = new Vector2(-1, y).normalized;

			//Set velocity with dir * speed
			GetComponent<Rigidbody2D>().velocity = dir * speed;
		}
	}
	float hitFactor(Vector2 ballPos, Vector2 racketPos, float racketHeight) {
		// ascii art:
		// \\1 <- at the top of the racket
		// \\0 <- at the middle of the racket
		// \\-1 <- at the bottom of the racket
		return (ballPos.y - racketPos.y) / racketHeight;
	}
}
